package main

import (
	"net/http"
	"path"
	"strings"
)

// FileServer is much like the standard FileServer, except it accounts for URLs
// without .html suffixes.
func FileServer(fs http.FileSystem) http.HandlerFunc {
	handler := http.FileServer(fs)
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// For paths without extensions, append `.html`.
		if r.URL.Path != "/" && !strings.HasSuffix(r.URL.Path, ".html") && !strings.ContainsAny(path.Base(r.URL.Path), ".") {
			r.URL.Path += ".html"
		}
		handler.ServeHTTP(w, r)
	})
}
