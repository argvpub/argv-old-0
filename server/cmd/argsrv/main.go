// argvsrv implements the application server.
package main

import (
	"flag"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

func main() {
	addr := flag.String("addr", "localhost:8000", "address to listen on")
	dev := flag.Bool("dev", false, "run in development mode")
	webdir := flag.String("webdir", "", "path to compiled static web assets")
	flag.Parse()

	if *dev {
		log.Println("Running in development mode")
	}

	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.Logger)

	r.Get("/x", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("this is the login page"))
	})
	r.Get("/*", FileServer(http.Dir(*webdir)))

	err := http.ListenAndServe(*addr, r)
	if err != nil {
		log.Fatalf("Could not listen to %s: %s", *addr, err.Error())
	}
}
