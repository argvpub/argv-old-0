// Package user provides unified user management.
//
// Each user maps to a single human being. There are explicit facilities for
// logical groups of multiple humans; it is never intended for multiple humans
// to share a single user.
//
// Bot and organization users are also treated specially.
package user

type User struct {
	ID   string
	Name string
}

func FromDB() (User, error) {
	return User{}, nil
}
