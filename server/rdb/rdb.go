// Package rdb provides implementation-agnostic interfaces to relational
// databases.
//
// By default, this package provides drivers for PostgreSQL and SQLite.
package rdb
