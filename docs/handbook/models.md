# Data modelling

Last edited: 6:50 PM PT, 26 August, 2018
Author: @liftm

Whenever we design a system, there are really three distinct _physical_ data
models we need to consider for each _logical_ data model.

When I say _physical_ data model, I mean the actual implementation of a data
model. This description generally includes its physical implementation and
representation in memory. For example, a User `struct` would be a _physical_
data model of User (possibly one of many).

A _logical_ data model is a single coherent concept in the system's ontology.
For example, although a User may have different underlying representations in
different components of a single logical system, the concept of a "user" is a
single _logical_ data model.

In general, a single logical data model has at least 3 distinct physical
implementations:

1. The client data model. This is shaped to make useful visualizations easier.
   It often contains precomputed aggregations and joins.
2. The server data model.
3. The database data model. This is optimized for storage querying and
   performance.
