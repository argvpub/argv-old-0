# argv

`argv` is the monorepo for `argv.pub` code and data.

## Developing `argv`

```bash
# Install `serve` (if not already available)
go get -u -v github.com/fogleman/serve

# Switch to `web` folder
cd web

# Compile pages when inputs change.
pug --watch *.pug

# Serve page locally
serve
```
